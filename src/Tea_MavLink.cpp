#include <Tea_MavLink.hpp>
//#include <Arduino.h>


TEA_MAVLINK::TEA_MAVLINK()
{
}

void TEA_MAVLINK::heartbeat()
{
  mavlink_message_t msg;
  // initialize the required buffers
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  // pack the message
  mavlink_msg_heartbeat_pack(system_id, component_id, &msg, system_type, autopilot_type, system_mode, custom_mode, system_state);
  // copy the message to the send buffer
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);

  mavSerial.write(buf, len);
  Serial.println("Heatbeat was sent.");
}

void TEA_MAVLINK::Mav_Request_Data()
{
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  static const Stream_params PROGMEM MAVStreams[] = {
      {MAV_DATA_STREAM_RAW_SENSORS, 1},
      {MAV_DATA_STREAM_EXTENDED_STATUS, 1},
      {MAV_DATA_STREAM_RC_CHANNELS, 1},
      {MAV_DATA_STREAM_RAW_CONTROLLER, 1},
      {MAV_DATA_STREAM_POSITION, 1},
      {MAV_DATA_STREAM_EXTRA1, 1},
      {MAV_DATA_STREAM_EXTRA2, 1},
      {MAV_DATA_STREAM_EXTRA3, 1}

  };

  for (uint8_t i = 0; i < sizeof(MAVStreams) / sizeof(Stream_params); i++)
  {
    uint8_t rate = pgm_read_byte(&(MAVStreams[i].rate));

    mavlink_msg_request_data_stream_pack(system_id, component_id, &msg, target_system, target_component, pgm_read_byte(&(MAVStreams[i].stream)), rate, 1);
  }
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavSerial.write(buf, len);

  requestDone = true;
  Serial.println("Data requested");
}

int TEA_MAVLINK::receive_mav()
{

  mavlink_message_t msg;
  mavlink_status_t status;

  while (mavSerial.available() > 0)
  {
    uint8_t c = mavSerial.read();
    if (mavlink_parse_char(MAVLINK_COMM_0, c, &msg, &status))
    {
      if (msg.sysid == 1 && msg.compid == 1)
      {
        //if(1==1){
        // Handle message {

        switch (msg.msgid)
        {
        case MAVLINK_MSG_ID_HEARTBEAT: // #0: Heartbeat

        {
          mavlink_heartbeat_t heartbeat;
          mavlink_msg_heartbeat_decode(&msg, &heartbeat);
          int armState = (heartbeat.base_mode & MAV_MODE_FLAG_SAFETY_ARMED);
          if (armState == MAV_MODE_FLAG_SAFETY_ARMED)
          {
            armed = true;
          }
          else
          {
            armed = false;
          }
          custom_mode = heartbeat.custom_mode;
          type = heartbeat.type;
          autopilot = heartbeat.autopilot;
#ifdef hbDebug
          Serial.println("Heartbeat_received");
          Serial.print("SysId_received:");
          Serial.println(msg.sysid);
#endif

#ifdef modeDebug
          Serial.print("mode:");
          FlightMode = custom_mode >> 16;
          Serial.print(FlightMode);

          Serial.print("  Type:");
          Serial.print(type);
          Serial.print("  Autopilot:");
          Serial.println(autopilot);
#endif
          base_mode = heartbeat.base_mode;
          system_status = heartbeat.system_status;

          if ((system_status & MAV_STATE_STANDBY) == MAV_STATE_STANDBY)
          {
            canFly = true;
          }
          else
          {
            canFly = false;
          }
#ifdef SystemstatusDebug
          Serial.print("____BaseMode_____");
          Serial.print(base_mode);
          Serial.print("____SystemStatus_____");
          Serial.println(system_status);
          if (canFly == true)
          {
            Serial.println("Drone can start now!");
          }
          else
          {
            Serial.println("Drone can not fly");
          }

#endif

#ifdef armDebug
          if (armed == true)
          {
            Serial.println("Armed");
          }

          else if (armed == false)
          {
            Serial.println("Disarmed");
          }
#endif
        }
        break;

        case MAVLINK_MSG_ID_VIBRATION:
        {
          mavlink_vibration_t vibrations;
          mavlink_msg_vibration_decode(&msg, &vibrations);
          vibration_x = vibrations.vibration_x;//mavlink_msg_vibration_get_vibration_x(&msg);
          vibration_y = vibrations.vibration_y;//mavlink_msg_vibration_get_vibration_y(&msg);
          vibration_z = vibrations.vibration_z;//mavlink_msg_vibration_get_vibration_z(&msg);
#ifdef vibDebug
          Serial.print("Vibration Z: ");
          Serial.print(vibrations.vibration_z);
          Serial.print("  Vibration X: ");
          Serial.print(vibrations.vibration_x);
          Serial.print("  Vibration Y: ");
          Serial.println(vibrations.vibration_y);
#endif
        }
        break;

        // case MAVLINK_MSG_ID_HIGHRES_IMU:
        // {
        //   mavlink_highres_imu_t ImuHighres;
        //   mavlink_msg_highres_imu_decode(&msg, &ImuHighres);
        //   temperature = ImuHighres.temperature;
        //   Serial.print("IMU_Temp :");
        //   Serial.println(temperature);
        // }
        // case MAVLINK_MSG_ID_HIGH_LATENCY2:
        // {
        //   mavlink_high_latency2_t highLat2;
        //   mavlink_msg_high_latency2_decode(&msg, &highLat2);
        //   Serial.print("Windspeed: ");
        //   Serial.print(highLat2.windspeed);
        //   Serial.print("  Windheading: ");
        //   Serial.println(highLat2.wind_heading);
        //   //Serial.println("HIGH_LATENCY_2_MESSAGE");
        // }

        case MAVLINK_MSG_ID_GPS_RAW_INT:

        {
          mavlink_gps_raw_int_t RawGps;
          mavlink_msg_gps_raw_int_decode(&msg, &RawGps);
          fix_type = RawGps.fix_type;
          // 0 = No GPS, 1 =No Fix, 2 = 2D Fix, 3 = 3D Fix
          sat_visible = RawGps.satellites_visible;
          hdop = RawGps.eph; //in centimeters so multiplication not needed
          _vel = RawGps.vel;
          lat_org = RawGps.lat; // store the orignal data
          lon_org = RawGps.lon;
          lat = lat_org / 10000000.0f;
          lon = lon_org / 10000000.0f;
#ifdef gpsDebug
          Serial.print("NumSat :");
          Serial.println(sat_visible);
#endif
        }
        break;

        case MAVLINK_MSG_ID_ALTITUDE:

        {
          mavlink_altitude_t altitude;
          mavlink_msg_altitude_decode(&msg, &altitude);
          alt = altitude.altitude_relative;
#ifdef altDebug
          Serial.print("ALT: ");
          Serial.println(alt);
#endif
        }
        break;

        case MAVLINK_MSG_ID_GLOBAL_POSITION_INT:
        {
          // MAVLINK_MSG_ID_GLOBAL_POSITION_INT globPos;
          // mavlink_msg_global_position_int_decode(&msg, &globPos)
          //alt =mavlink_msg_global_position_int_get_relative_alt(&msg);
        }
        break;

        case MAVLINK_MSG_ID_DISTANCE_SENSOR:
        {
          distanceSensor = mavlink_msg_distance_sensor_get_current_distance(&msg);
        }
        break;

        case MAVLINK_MSG_ID_SYS_STATUS:
        {
          //                   mavlink_sys_status_t systemStatus;
          //                   mavlink_msg_sys_status_decode(&msg,&systemStatus);
          onboard_control_sensors_health = mavlink_msg_sys_status_get_onboard_control_sensors_health(&msg);

#ifdef sysStatusDebug
          if ((onboard_control_sensors_health & MAV_SYS_STATUS_SENSOR_RC_RECEIVER) != MAV_SYS_STATUS_SENSOR_RC_RECEIVER)
          {
            Serial.println(F("RC_Receiver_Fail; "));
          }

          if ((onboard_control_sensors_health & MAV_SYS_STATUS_SENSOR_BATTERY) == MAV_SYS_STATUS_SENSOR_BATTERY)
          {
            Serial.println(F("BattSensorFail; "));
          }
          if ((onboard_control_sensors_health & MAV_SYS_STATUS_LOGGING) != MAV_SYS_STATUS_LOGGING)
          {
            logging = true;
            Serial.println(F("LoggingError; "));
          }
          else
          {
            logging = false;
          }
#endif

          //Serial.print("SensorHealth"); Serial.println(onboard_control_sensors_health);

          voltage = (mavlink_msg_sys_status_get_voltage_battery(&msg)); //Battery voltage, in millivolts (1 = 1 millivolt)
          amp = (mavlink_msg_sys_status_get_current_battery(&msg));     //Battery current, in 10*milliamperes (1 = 10 milliampere)         10
          CellVolt = voltage / 6;

#ifdef battDebug
          Serial.print("Cell Voltage :");
          Serial.print(CellVolt);
          Serial.print(" Voltage :");
          Serial.println(voltage);
#endif
        }
        break;

        case MAVLINK_MSG_ID_BATTERY_STATUS:

        {
          mavlink_battery_status_t battStat;
          mavlink_msg_battery_status_decode(&msg, &battStat);

          amp = battStat.current_battery;
          voltage = mavlink_msg_battery_status_get_voltages(&msg, &voltages[1]); //Prüfen of das so geht.
#ifdef battDebug
          Serial.print("currentConsumed: ");
          Serial.println(battStat.battery_remaining);
#endif

          capacity = mavlink_msg_battery_status_get_current_consumed(&msg);

          //numCounts = 0; //todo: what's this for??
        }
        break;

        case MAVLINK_MSG_ID_STATUSTEXT:
        {

          mavlink_statustext_t packet;
          mavlink_msg_statustext_decode(&msg, &packet);
          memcpy(&text, packet.text, MAVLINK_MSG_STATUSTEXT_FIELD_TEXT_LEN);
          Serial.print("Text: ");
          Serial.println(text);
          break;
        }

        case MAVLINK_MSG_ID_SYSTEM_TIME:
        {
          SysTime = mavlink_msg_system_time_get_time_boot_ms(&msg);
        }
        break;

          //

        case MAVLINK_MSG_ID_RC_CHANNELS:
        {

          mavlink_rc_channels_t Rc;
          mavlink_msg_rc_channels_decode(&msg, &Rc);
#ifdef rcDebug
          Serial.print("RC1:");
          Serial.println(Rc.chan1_raw);
#endif
        }

        case MAVLINK_MSG_ID_ATTITUDE:
        {

          mavlink_attitude_t atti;
          mavlink_msg_attitude_decode(&msg, &atti);

          __roll = atti.roll * 57.295779513;
          __pitch = atti.pitch * 57.295779513;
          __yaw = atti.yaw * 57.295779513;

          __attitudeAngleDegSum = sqrt((__pitch * __pitch) + (__roll * __roll));
          __windSpeed = __attitudeAngleDegSum * windspeedFactor;

#ifdef attitudeDebug
          Serial.print("Roll° : ");
          Serial.print(__roll);
          Serial.print("   Nick° : ");
          Serial.print(__pitch);
          Serial.print("   Heading° : ");
          Serial.print(__yaw);
          Serial.print("   Winkelsumme : ");
          Serial.print(__attitudeAngleDegSum);
          Serial.print("   WindSpeed Kmh: ");
          Serial.println(__windSpeed * 3.6);

#endif
        }
        break;

        case MAVLINK_MSG_ID_COMMAND_ACK:
          mavlink_command_ack_t messageStatus;
          {
            mavlink_msg_command_ack_decode(&msg, &messageStatus);
            Serial.print("MAVLINK_MSG_ID_COMMAND_ACK Command ID :");
            Serial.print(messageStatus.command);
            Serial.print("  Result :");
            Serial.println(messageStatus.result);
          }
          break;

        case MAVLINK_MSG_ID_PARAM_EXT_ACK:
        {
          mavlink_param_ext_ack_t ext_ack;
          mavlink_msg_param_ext_ack_decode(&msg, &ext_ack);
          Serial.print("Ack_result  :");
          Serial.print(ext_ack.param_result);
        }
        break;

        case MAVLINK_MSG_ID_COMMAND_LONG:
          mavlink_command_long_t messageStatusLong;
          {
            mavlink_msg_command_long_decode(&msg, &messageStatusLong);
            Serial.print("MAVLINK_MSG_ID_COMMAND_LONG command :");
            Serial.println(messageStatusLong.command);
          }
          break;

        case MAVLINK_MSG_ID_NAV_CONTROLLER_OUTPUT:
        {
          mavlink_nav_controller_output_t navController;
          mavlink_msg_nav_controller_output_decode(&msg, &navController);
          Serial.print("Distance to WP is :");
          Serial.println(navController.wp_dist);
        }
        break;

        case MAVLINK_MSG_ID_PARAM_VALUE:
        {
          mavlink_param_value_t param_value;
          mavlink_msg_param_value_decode(&msg, &param_value);
          paramFedBack = mavlink_msg_param_value_get_param_value(&msg);
          strcpy(paramFedbackName, (param_value.param_id));

          Serial.print(paramFedbackName);
          Serial.print("  tparamFedBack Vvlue:");
          Serial.println(paramFedBack);
        }
        break;
        }
      }
    }
  }
  return 0;
}

float TEA_MAVLINK::getVibrationX()
{
  return vibration_x;
}

float TEA_MAVLINK::getVibrationY()
{
  return vibration_y;
}

float TEA_MAVLINK::getVibrationZ()
{
  return vibration_z;
}

void TEA_MAVLINK::setParam(char param[16], int val)
{
  mavlink_message_t msg;
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  strcpy(param_id, (param));
  param_value = val;
  param_type = 9;

  mavlink_msg_param_set_pack(system_id, component_id, &msg, target_system, target_component, param_id, param_value, param_type);
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  // Send the message
  mavSerial.write(buf, len);
  Serial.print("Send Param  :");
  Serial.print(param);
  Serial.print("  ");
  Serial.println(param_value);
}

void TEA_MAVLINK::requestParam(char param[16])
{
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  mavlink_message_t msg;
  mavlink_msg_param_request_read_pack(system_id, component_id, &msg, target_system, target_component, param_id, -1);
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  // Send the message
  mavSerial.write(buf, len);
  Serial.print("Requested param  :");
  Serial.println(param);
}

void TEA_MAVLINK::requestList()
{
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  mavlink_message_t msg;
  mavlink_msg_param_request_list_pack(system_id, component_id, &msg, target_system, target_component);
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavSerial.write(buf, len);
  Serial.println("ParamList was requested");
}

void TEA_MAVLINK::setMessageIdInterval(uint16_t messageID, int32_t interval)
{
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  mavlink_message_t msg;
  mavlink_msg_message_interval_pack(system_id, component_id, &msg, messageID, interval);
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavSerial.write(buf, len);
}

void TEA_MAVLINK::requestStreamID()
{
  uint16_t req_message_rate = 1; /*< [Hz] The requested message rate*/
  uint8_t req_stream_id = 30;    /*<  The ID of the requested data stream*/
  uint8_t start_stop = 1;        /*<  1 to start sending, 0 to stop sending.*/

  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  mavlink_message_t msg;

  mavlink_msg_request_data_stream_pack(system_id, component_id, &msg, target_system, target_component, req_stream_id, req_message_rate, start_stop);
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavSerial.write(buf, len);
}

void TEA_MAVLINK::CommandLong(uint16_t command, uint8_t msgID, uint32_t msgRate)
{
  float param1; /*<  Parameter 1 (for the specific command).*/
  float param2; /*<  Parameter 2 (for the specific command).*/
  float param3; /*<  Parameter 3 (for the specific command).*/
  float param4; /*<  Parameter 4 (for the specific command).*/
  float param5; /*<  Parameter 5 (for the specific command).*/
  float param6; /*<  Parameter 6 (for the specific command).*/
  float param7; /*<  Parameter 7 (for the specific command).*/
  //uint16_t command; /*<  Command ID (of command to send).*/
  uint8_t confirmation; /*<  0: First transmission of this command. 1-255: Confirmation transmissions (e.g. for kill command)*/
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  if (command == MAV_CMD_SET_MESSAGE_INTERVAL)
  {
    msgRate = 50000 / msgRate;
  }
  mavlink_message_t msg;
  mavlink_msg_command_long_pack(system_id, component_id, &msg, target_system, target_component, command, confirmation, msgID, msgRate, 0, 0, 0, 0, 0); //msgID,us
  //mavlink_msg_command_long_pack(system_id,component_id,&msg,0,0,2500,confirmation,1,1,0,0,0,0,0);//msgID,us
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavSerial.write(buf, len);
}

void TEA_MAVLINK::MessageRequest(uint16_t command, float param1, float param2,float param3,float param4,float param5,float param6,float param7)
{

  uint8_t confirmation; /*<  0: First transmission of this command. 1-255: Confirmation transmissions (e.g. for kill command)*/
  uint8_t buf[MAVLINK_MAX_PACKET_LEN];
  if (command == MAV_CMD_SET_MESSAGE_INTERVAL)
  {
    param2 = 50000 / param2;
  }
  mavlink_message_t msg;
  //mavlink_msg_command_long_pack(system_id, component_id, &msg, target_system, target_component, command, confirmation, msgID, msgRate, 0, 0, 0, 0, 0); //msgID,us
  mavlink_msg_command_long_pack(system_id, component_id, &msg, target_system, target_component, command, confirmation, param1, param2, param3, param4, param5, param6, param7); //msgID,us
  //mavlink_msg_command_long_pack(system_id, component_id, &msg, target_system, target_component, MAV_CMD_DO_MOTOR_TEST, confirmation, 1,MOTOR_TEST_THROTTLE_PERCENT, 1, 0.2, 1, 0, param7); //msgID,us
  //mavlink_msg_command_long_pack(system_id,component_id,&msg,0,0,2500,confirmation,1,1,0,0,0,0,0);//msgID,us
  uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
  mavSerial.write(buf, len);
}
