#ifndef TEA_MAVLINK_hpp
#define TEA_MAVLINK_hpp
//#pragma once
#include <Arduino.h>
#include <standard/mavlink.h>

#define MAVLINK_COMM_NUM_BUFFERS 1
//#define mavSerial Serial1
#define mavSerial Serial2 
#define EEPROM_SIZE 30
//#define #dustDebug
//#define rcDebug
//#define debugSVP
//#define speedAngleFactor 0.96 //coVecdro 4,5m/s => 6°  => 0.75 rück 4.5m/s 4.5° =>1 // this must be crorected depending on the used drone!
//#define modeDebug
//#define armDebug
//#define vibDebug
//#define debugZacc
//#define SystemstatusDebug
//#define hbDebug
//#gpsDebug
//#define attitudeDebug
//#define printLoopTime
//#define messageSending //Send all Mavlink Messages to the vehicle RC-channels etc..g. only activate if you know what you are doing!
//#define sendRcOverride //enables RC-override to enable for example standby-Mode or take over in critical situations
//#define altDebug
//#define sendRcOverride  //RC will be overwritten
//#define battDebug
//#define sysStatusDebug
//#define paramDebug
#define setParamEnable

class TEA_MAVLINK
{
public:
    TEA_MAVLINK();
    //void initialize();
    void heartbeat();
    void Mav_Request_Data();
    int receive_mav();
    void setParam(char param[16], int val);
    void requestParam(char param[16]);
    void requestList();
    void setMessageIdInterval();
    void setMessageIdInterval(uint16_t messageID, int32_t interval);
    void requestStreamID();
    void CommandLong(uint16_t command, uint8_t msgID, uint32_t msgRate);
    float getVibrationX();
    float getVibrationY();
    float getVibrationZ();
    void MessageRequest(uint16_t command, float param1, float param2,float param3,float param4,float param5,float param6,float param7);

private:
    #pragma pack(push, 1)
    typedef struct _Stream_params
    {
        uint8_t stream;
        uint8_t rate;
    } Stream_params;
    uint8_t system_id = 42;    //systhem ID of this system
    uint8_t component_id = 32; //who is sending the information?
    uint8_t target_system = 1;
    uint8_t target_component = 0;
    //int numCounts;
    uint8_t system_type = MAV_TYPE_GCS; // define the system type, in this case ground control station
    uint8_t autopilot_type = MAV_AUTOPILOT_INVALID;
    uint8_t system_mode = 0;
    uint32_t custom_mode = 0;
    uint8_t system_state = 1;
    uint8_t base_mode = 1; ///< The new base mode
    uint8_t type;
    uint8_t autopilot;
    uint8_t LastActiveFc;
    uint16_t mavLinkFail;
    bool requestDone;
    uint8_t rssi;
    uint8_t failsafe;
    bool geoFenceBreach;
    bool logging;
    bool rcOverrideDone;

    bool armed;
    bool canFly;
    uint32_t ultraslowLoop;

    uint32_t lastSysTime;
    uint32_t flightTime;
    uint16_t flightMinute;
    uint16_t _vel;

    // Mavlink variables
    uint8_t system_status;
    uint8_t currentmode;
    float vibration_x = 0;
    float vibration_y = 0;
    float vibration_z = 0;

    int verticalSpeed;
    int lastAlt;
    uint16_t voltages[10];
    uint16_t voltage;
    uint16_t CellVolt;
    uint16_t amp;

    uint8_t fix_type;
    uint8_t sat_visible;
    uint8_t hdop;
    uint8_t groundspeed;
    float alt;
    long compensatedAlt;
    uint16_t distanceSensor;
    uint16_t capacity;
    uint16_t temperature;
    uint32_t SysTime;
    char jetitext[16];
    char jetitext2[16];
    word message1;
    word message2;
    char text[50];

    uint32_t onboard_control_sensors_health;

    float velocity_variance;    /*< Velocity variance*/
    float pos_horiz_variance;   /*< Horizontal Position variance*/
    float pos_vert_variance;    /*< Vertical Position variance*/
    float compass_variance;     /*< Compass variance*/
    float terrain_alt_variance; /*< Terrain Altitude variance*/

    uint16_t flags; /*< Flags*/

    uint16_t compass_variance_filt;
    uint16_t velocity_variance_filt;
    uint16_t pos_horiz_variance_filt;
    uint16_t pos_vert_variance_filt;
    uint16_t terrain_alt_variance_filt;

    uint16_t ekfEstFlag;

    //vars for Distance
    float lat = 0.00f; // latidude (i.e. -48.600000f)
    float lon = 0.00f; // longitude
    long lat_org = 0;  // stores the received coordinate in its original high resolution form (i.e. -486000000)
    long lon_org = 0;

    //static uint8_t      fix_type = 0;               // GPS lock 0-1=no fix, 2=2D, 3=3D
    int16_t ap_gps_hdop = 0;  //hdop value
    int8_t fix_type_jeti = 0; // GPS lock 0=no GPS, 1=no fix, 2=2D, 3=3D
    int16_t home_distance = 0;

    //particle
    uint16_t particlePin;
    float dustDensity;
    //___________________Angle
    int __accx = 0;
    int __accy = 0;
    int __accz = 0;
    int __gyrox = 0;
    int __gyroy = 0;
    int __gyroz = 0;

    float __attitudePitchDeg = 0.0f;
    float __attitudeRollDeg = 0.0f;
    float __attitudePitchRad = 0.0f;
    float __attitudeRollRad = 0.0f;
    float __attitudeAngleDegSum = 0.0f;
    //______________Atti
    float __roll;  ///< Roll angle (rad)
    float __pitch; ///< Pitch angle (rad)
    float __yaw;   ///< Yaw angle (rad)
                   //int __attitudeAngleDegSum;

    //__________Wind
    int __windSpeed;


    #define windspeedFactor 1;

    float humidityTemperatur;
    float humidity;
    unsigned long loopTime;

    char paramFedbackName[16];
    float paramFedBack;

    //#ifdef setParamEnable
    // Parameter
    float param_value;
    char param_id[16];
    uint8_t param_type;
    //#endif //setParamEnable

    uint32_t FlightMode;

    //PX4

    //enum PX4_CUSTOM_MAIN_MODE {
    //  PX4_CUSTOM_MAIN_MODE_MANUAL = 1,
    //  PX4_CUSTOM_MAIN_MODE_ALTCTL,
    //  PX4_CUSTOM_MAIN_MODE_POSCTL,
    //  PX4_CUSTOM_MAIN_MODE_AUTO,
    //  PX4_CUSTOM_MAIN_MODE_ACRO,
    //  PX4_CUSTOM_MAIN_MODE_OFFBOARD,
    //  PX4_CUSTOM_MAIN_MODE_STABILIZED,
    //  PX4_CUSTOM_MAIN_MODE_RATTITUDE,
    //  PX4_CUSTOM_MAIN_MODE_SIMPLE /* unused, but reserved for future use */
    //};
    //
    //enum PX4_CUSTOM_SUB_MODE_AUTO {
    //  PX4_CUSTOM_SUB_MODE_AUTO_READY = 1,
    //  PX4_CUSTOM_SUB_MODE_AUTO_TAKEOFF,
    //  PX4_CUSTOM_SUB_MODE_AUTO_LOITER,
    //  PX4_CUSTOM_SUB_MODE_AUTO_MISSION,
    //  PX4_CUSTOM_SUB_MODE_AUTO_RTL,
    //  PX4_CUSTOM_SUB_MODE_AUTO_LAND,
    //  PX4_CUSTOM_SUB_MODE_AUTO_RESERVED_DO_NOT_USE, // was PX4_CUSTOM_SUB_MODE_AUTO_RTGS, deleted 2020-03-05
    //  PX4_CUSTOM_SUB_MODE_AUTO_FOLLOW_TARGET,
    //  PX4_CUSTOM_SUB_MODE_AUTO_PRECLAND
    //};
};

#endif
