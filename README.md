# README #

This library contains functions and data structures to allow easy interaction with MavLink devices. 

### Notes ###

MavLink broadcast must be enabled in Px4 to make devices visible to all components!